import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AlertController, LoadingController } from '@ionic/angular';
import { CarRecordsService } from '../car-records/car-records.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  carList = ['Honda City', 'Hyundai i20', 'Toyota Innova', 'Audi A4'];
  dealers = ['Rahul', 'Abhishek', 'Pooja'];

  constructor(
    private carRecordsService: CarRecordsService,
    private loadingController: LoadingController,
    private alertController: AlertController
  ) {}

  onSubmit(form: NgForm) {
    this.loadingController
      .create({
        keyboardClose: true,
        message: 'Adding Car Details',
      })
      .then((loadingEl) => {
        loadingEl.present();
        this.carRecordsService
          .addCarRecord(
            form.form.value.carModel,
            form.form.value.customerName,
            form.form.value.discount,
            form.form.value.approvalFrom
          )
          .subscribe(() => {
            console.log('car record added');
          });
        loadingEl.dismiss();
        form.reset();
      });
    this.alertController
      .create({
        header: 'Success',
        message: 'Car Details Added',
        buttons: ['Okay'],
      })
      .then((alertEl) => {
        alertEl.present();
      });
  }
}
