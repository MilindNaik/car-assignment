import { Injectable } from '@angular/core';
import { ICarModel } from './car-record.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class CarRecordsService {
  carRecords: ICarModel[] = [
    new ICarModel('Honda City', 'Milind', 4000, 'Rahul'),
    new ICarModel('Hyundai i20', 'Mohit', 5000, 'Pooja'),
  ];

  constructor(private http: HttpClient) {}

  getAllRecords() {
    return [...this.carRecords];
  }

  addCarRecord(
    carModel: string,
    customerName: string,
    discount: number,
    approvalFrom: string
  ) {
    const newRecord = new ICarModel(
      carModel,
      customerName,
      discount,
      approvalFrom
    );
    return this.http.post<any>(`https://localhost:44398/api/carRecord`, {
      ...newRecord,
    });
  }

  editCarRecord(
    id: any,
    carModel: string,
    customerName: string,
    discount: number,
    approvalFrom: string
  ) {
    const editedRecord = new ICarModel(
      carModel,
      customerName,
      discount,
      approvalFrom
    );
    return this.http.put<any>(`https://localhost:44398/api/carRecord/${id}`, {
      ...editedRecord,
    });
  }

  getCarRecords() {
    let url = `https://localhost:44398/api/carRecord`;
    return this.http.get(url);
  }

  getSingleCarRecord(id: any) {
    return this.http.get(`https://localhost:44398/api/carRecord/${id}`);
  }

  delete(id: any) {
    return this.http.delete(`https://localhost:44398/api/carRecord/${id}`);
  }
}
