

export class ICarModel {
  constructor(
    public carModel: string,
    public customerName: string,
    public discount: number,
    public approvalFrom: string
  ) {}
}
