import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { ICarModel } from './car-record.model';
import { CarRecordsService } from './car-records.service';

@Component({
  selector: 'app-car-records',
  templateUrl: './car-records.page.html',
  styleUrls: ['./car-records.page.scss'],
})
export class CarRecordsPage implements OnInit {
  records: ICarModel[];
  fetchedCarRecords: any;
  constructor(
    private carRecordsService: CarRecordsService,
    private alertControl: AlertController,
    private router: Router
  ) {
    // this.carRecordsService.getCarRecords().subscribe((data) => {
    //   this.fetchedCarRecords = data;
    //   console.log('data from .net', this.fetchedCarRecords);
    // });
  }

  ngOnInit() {}

  ionViewWillEnter() {
    this.carRecordsService.getCarRecords().subscribe((data) => {
      this.fetchedCarRecords = data;
      console.log('data from .net', this.fetchedCarRecords);
    });
  }

  editCarRecord(id: any) {
    this.router.navigateByUrl(`car-records/${id}`);
  }

  deleteCarRecord(id: any) {
    this.alertControl
      .create({
        header: 'Are you sure?',
        message: 'Do you really want to delete this record?',
        buttons: [
          { text: 'Cancel', role: 'cancel' },
          {
            text: 'Delete',
            handler: () => {
              this.carRecordsService.delete(id).subscribe(() => {
                console.log('delete successful');
                this.carRecordsService.getCarRecords().subscribe((data) => {
                  this.fetchedCarRecords = data;
                  console.log(
                    'Data reloaded after deletion',
                    this.fetchedCarRecords
                  );
                });
              });
            },
          },
        ],
      })
      .then((alertEl) => {
        alertEl.present();
      });
  }
}
