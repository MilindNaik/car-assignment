import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CarRecordsPageRoutingModule } from './car-records-routing.module';

import { CarRecordsPage } from './car-records.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CarRecordsPageRoutingModule
  ],
  declarations: [CarRecordsPage]
})
export class CarRecordsPageModule {}
