import { TestBed } from '@angular/core/testing';

import { CarRecordsService } from './car-records.service';

describe('CarRecordsService', () => {
  let service: CarRecordsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CarRecordsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
