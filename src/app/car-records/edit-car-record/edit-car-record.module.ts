import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditCarRecordPageRoutingModule } from './edit-car-record-routing.module';

import { EditCarRecordPage } from './edit-car-record.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditCarRecordPageRoutingModule
  ],
  declarations: [EditCarRecordPage]
})
export class EditCarRecordPageModule {}
