import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditCarRecordPage } from './edit-car-record.page';

const routes: Routes = [
  {
    path: '',
    component: EditCarRecordPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditCarRecordPageRoutingModule {}
