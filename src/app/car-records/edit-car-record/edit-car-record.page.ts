import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AlertController, LoadingController } from '@ionic/angular';
import { CarRecordsService } from '../car-records.service';

@Component({
  selector: 'app-edit-car-record',
  templateUrl: './edit-car-record.page.html',
  styleUrls: ['./edit-car-record.page.scss'],
})
export class EditCarRecordPage implements OnInit {
  loadedCarRecord: any;
  isLoading = false;
  carList = ['Honda City', 'Hyundai i20', 'Toyota Innova', 'Audi A4'];
  dealers = ['Rahul', 'Abhishek', 'Pooja'];

  constructor(
    private activatedRoute: ActivatedRoute,
    private carRecordsService: CarRecordsService,
    private loadingController: LoadingController,
    private alertController: AlertController
  ) {}

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe((paramMap) => {
      if (!paramMap.has('carId')) {
        return;
      }
      const carRecordId = paramMap.get('carId');
      this.isLoading = true;
      this.carRecordsService
        .getSingleCarRecord(carRecordId)
        .subscribe((data) => {
          this.loadedCarRecord = data;
          console.log('loaded car data');
          console.log('Loaded Car record', this.loadedCarRecord);
          this.isLoading = false;
        });
    });
  }

  

  onSubmit(id: any, form: NgForm) {
    this.loadingController
      .create({
        keyboardClose: true,
        message: 'Editing Record',
      })
      .then((loadingEl) => {
        loadingEl.present();
        this.carRecordsService
          .editCarRecord(
            id,
            form.form.value.carModel,
            form.form.value.customerName,
            form.form.value.discount,
            form.form.value.approvalFrom
          )
          .subscribe(() => {
            console.log('car record edited');
          });
        loadingEl.dismiss();
        form.reset();
      });
    this.alertController
      .create({
        header: 'Success',
        message: 'Car Details Edited',
        buttons: ['Okay'],
      })
      .then((alertEl) => {
        alertEl.present();
      });
  }
}
