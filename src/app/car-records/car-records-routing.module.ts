import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CarRecordsPage } from './car-records.page';

const routes: Routes = [
  {
    path: '',
    component: CarRecordsPage,
  },
  {
    path: ':carId',
    loadChildren: () =>
      import('./edit-car-record/edit-car-record.module').then(
        (m) => m.EditCarRecordPageModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CarRecordsPageRoutingModule {}
