import { Injectable } from '@angular/core';
import { IUser } from './user.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private _isLogin = true;

  get loginState() {
    return this._isLogin;
  }

  constructor(private http: HttpClient) {}

  addUser(userName: string, password: string) {
    const newUser = new IUser(userName, password);
    return this.http.post<any>(`https://localhost:44398/api/user`, {
      ...newUser,
    });
  }

  loginUser(userName: string, password: string) {
    const userToLogin = new IUser(userName, password);
    return this.http.post<any>(`https://localhost:44398/api/user/login`, {
      ...userToLogin,
    });
  }

  login() {
    this._isLogin = true;
  }

  logout() {
    this._isLogin = false;
  }
}
