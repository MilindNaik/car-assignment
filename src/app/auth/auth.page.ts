import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, LoadingController } from '@ionic/angular';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.page.html',
  styleUrls: ['./auth.page.scss'],
})
export class AuthPage implements OnInit {
  isValidUser = false;
  isLoginPage = true;
  constructor(
    private authService: AuthService,
    private router: Router,
    private loadingController: LoadingController,
    private alertController: AlertController
  ) {}

  switchPage() {
    this.isLoginPage = !this.isLoginPage;
    console.log('isLoginPage', this.isLoginPage);
  }

  ngOnInit() {}

  onSubmit(form: NgForm) {
    if (this.isLoginPage) {
      this.loadingController
        .create({
          keyboardClose: true,
          message: 'Logging in...',
        })
        .then((loadingEl) => {
          loadingEl.present();
          this.authService
            .loginUser(form.form.value.username, form.form.value.password)
            .subscribe((data) => {
              this.isValidUser = data;
              console.log('is User valid?', this.isValidUser);
              if (this.isValidUser) {
                this.authService.login();
                this.router.navigateByUrl('/home');
                loadingEl.dismiss();
                form.reset();
              } else {
                this.alertController
                  .create({
                    header: 'Error',
                    message: 'Invalid Credentials',
                    buttons: ['Okay'],
                  })
                  .then((alertEl) => {
                    loadingEl.dismiss();
                    alertEl.present();
                    form.reset();
                  });
              }
            });
        });
    }
    if (!this.isLoginPage) {
      this.loadingController
        .create({
          keyboardClose: true,
          message: 'Logging in...',
        })
        .then((loadingEl) => {
          loadingEl.present();
          this.authService
            .addUser(form.form.value.username, form.form.value.password)
            .subscribe(() => {
              console.log('user added');
              loadingEl.dismiss();
            });
        });
      this.alertController
        .create({
          header: 'Success',
          message: 'User Registered',
          buttons: ['Okay'],
        })
        .then((alertEl) => {
          alertEl.present();
          form.reset();
        });
    }
  }
}
