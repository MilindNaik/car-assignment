import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class SaleRecordsService {
  constructor(private http: HttpClient) {}

  getSalesRecords(approvalFrom: any) {
    return this.http.get(
      `https://localhost:44398/api/carRecord/saleRecord?approvalFrom=${approvalFrom}`
    );
  }
}
