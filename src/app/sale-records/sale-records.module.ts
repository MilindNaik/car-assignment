import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SaleRecordsPageRoutingModule } from './sale-records-routing.module';

import { SaleRecordsPage } from './sale-records.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SaleRecordsPageRoutingModule
  ],
  declarations: [SaleRecordsPage]
})
export class SaleRecordsPageModule {}
