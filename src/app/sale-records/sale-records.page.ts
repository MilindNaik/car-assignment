import { Component, OnInit } from '@angular/core';
import { SaleRecordsService } from './sale-records.service';

@Component({
  selector: 'app-sale-records',
  templateUrl: './sale-records.page.html',
  styleUrls: ['./sale-records.page.scss'],
})
export class SaleRecordsPage implements OnInit {
  approvalFrom: string;
  filteredRecords: any;
  dealers = ['Rahul', 'Abhishek', 'Pooja'];

  constructor(private salesRecordsService: SaleRecordsService) {}

  ngOnInit() {}

  filterSaleData() {
    this.salesRecordsService
      .getSalesRecords(this.approvalFrom)
      .subscribe((data) => {
        this.filteredRecords = data;
        console.log('filteredRecords', this.filteredRecords);
      });
  }

  // ngOnInit() {
  //   this.salesRecordsService
  //     .getSalesRecords(this.approvalFrom)
  //     .subscribe((data) => {
  //       if (data !== this.filteredRecords) {
  //         this.filteredRecords = data;
  //       }
  //     });
  // }
}
