import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SaleRecordsPage } from './sale-records.page';

const routes: Routes = [
  {
    path: '',
    component: SaleRecordsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SaleRecordsPageRoutingModule {}
