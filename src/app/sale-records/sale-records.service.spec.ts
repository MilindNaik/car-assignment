import { TestBed } from '@angular/core/testing';

import { SaleRecordsService } from './sale-records.service';

describe('SaleRecordsService', () => {
  let service: SaleRecordsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SaleRecordsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
